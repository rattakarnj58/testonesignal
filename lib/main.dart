import 'package:flutter/material.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

void main(){
  WidgetsFlutterBinding.ensureInitialized();
  OneSignal.shared.init(
      "e584fe8f-2093-4830-837f-2e261da7f87e",
      iOSSettings: null);
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title1: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title1}) : super(key: key);

  final String title1;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
String title = "title";
String body = "body";

  @override
  void initState(){
    super.initState();
    OneSignal.shared.setNotificationReceivedHandler((OSNotification notification) {
      setState(() {
        title = notification.payload.title;
        body = notification.payload.body;
        print(body);
      });
    });

    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      print("Notificaytion");
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              title,
            ),
            Text(
              body,
            ),
          ],
        ),
      ),
    );
  }
}
